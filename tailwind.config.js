module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
        colors:{
            'gravel': {
                '50': '#f6f6f6',
                '100': '#ededee',
                '200': '#d3d2d4',
                '300': '#b8b6ba',
                '400': '#828086',
                '500': '#4d4952',
                '600': '#45424a',
                '700': '#3a373e',
                '800': '#2e2c31',
                '900': '#262428'
            }
        },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [require('@tailwindcss/forms'),]
}
