<?php

namespace App\Http\Controllers;

use App\Models\Calendar;
use App\Http\Resources\CalendarResource;
use Carbon\Carbon;
use DB;


use Illuminate\Http\Request;

class EventController extends Controller
{

    public function index()
    {
        $event = Calendar::first();

        if( !$event ){
            return response()->json([
                'message' => 'No events'
            ]);
        }


        $from = Carbon::parse($event->start_date);
        $to = Carbon::parse($event->end_date);

        $events = [];
        $selectedDaysInArray = json_decode($event->selected_days);

        for($from; $from <= $to; $from->addDay() ) {
            # code...
            if( in_array($from->dayOfWeek, $selectedDaysInArray) ){
                array_push($events, [
                    'title' => $event->name,
                    'date' => $from->format('Y-m-d')
                ]);
            }
        }


        return response()->json($events);
    }

    public function store (Request $request){

        Calendar::truncate();

        $calendar = new Calendar;
        $calendar->name = $request->name;
        $calendar->start_date = $request->start_date;
        $calendar->end_date = $request->end_date;
        $calendar->selected_days = json_encode($request->selected_days);
        $calendar->save();

        return response()->json($calendar);
    }

}
